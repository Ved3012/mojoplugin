package com;
public class Bus extends Vehicle {
	
	public Bus()
	{
		System.out.println("Constructor from the bus class called...");
		
	}
	
	public void drive()
	{
		System.out.println("Drive method in bus class called....");
	}
}
