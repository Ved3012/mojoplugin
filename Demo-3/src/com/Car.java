package com;

public class Car extends Vehicle{
	
	
	public Car()
	{
		System.out.println("Constructor of car called...");
	}
	
	
	public void drive()
	{
		System.out.println("Drive function in car class is called...");
	}
}
